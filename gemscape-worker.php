<?php

if( function_exists('acf_add_local_field_group') ):
 acf_add_local_field_group(array (
   'key' => 'group_554dee05s593d',
   'title' => 'Gemscape Boss Settings',
   'fields' => array (
     array (
       'key' => 'field_554ef302cff5f',
       'label' => 'Boss URL',
       'name' => 'gemscape_boss_url',
       'type' => 'url',
       'instructions' => '',
       'required' => 1,
       'conditional_logic' => 0,
       'wrapper' => array (
         'width' => '',
         'class' => '',
         'id' => '',
       ),
       'default_value' => '',
       'placeholder' => 'http://',
     ),
   ),
   'location' => array (
     array (
       array (
         'param' => 'options_page',
         'operator' => '==',
         'value' => 'gemscape-settings',
       ),
     ),
   ),
   'menu_order' => 2,
   'position' => 'normal',
   'style' => 'seamless',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
 ));
 endif;

  $boss_url = get_field('gemscape_boss_url', 'option');
  $area_json_url = $boss_url . '/wp-json/taxonomies/area/terms';
  if($boss_url != NULL && function_exists('acf_add_local_field_group')){
		$areas = slug_get_json($area_json_url);
	  $site_areas = array();
	  foreach( $areas as $area ) {
	    $site_areas[$area->slug] = $area->slug;
	  }
    acf_add_local_field_group(array (
      'key' => 'group_554dee09s593d',
      'title' => 'Gemscape Site Area Settings',
      'fields' => array (
        array (
        'key' => 'field_554ef330cff60',
        'label' => 'Site Areas',
        'name' => 'gemscape_worker_areas',
        'type' => 'select',
        'instructions' => 'Select areas your site covers by default.',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'choices' => $site_areas,
        'default_value' => array (
          '' => '',
        ),
        'allow_null' => 0,
        'multiple' => 1,
        'ui' => 1,
        'ajax' => 0,
        'placeholder' => '',
        'disabled' => 0,
        'readonly' => 0,
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'gemscape-settings',
          ),
        ),
      ),
      'menu_order' => 3,
      'position' => 'normal',
      'style' => 'seamless',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    ));
  }

require_once('lib/shortcodes.php');
