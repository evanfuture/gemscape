<?php
/**
 * Plugin Name: Gemscape
 * Plugin URI: http://worldscape.co/gemscape
 * Description: A plugin for creating and managing Gems.
 * Version: 1.0.0
 * Author: Evan Payne
 * Author URI: http://evanpayne.com
 * Text Domain: gemscape
 * Domain Path: /languages
 */

function gemscape_acf_check() {
   if ( is_admin() && current_user_can( 'activate_plugins' ) &&  !is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
     $message = '<div class="error"><p>Sorry, but Gemscape requires the Advanced Custom Fields plugin to be installed and active.</p></div>';
     return $message;
     deactivate_plugins( plugin_basename( __FILE__ ) );

     if ( isset( $_GET['activate'] ) ) {
       unset( $_GET['activate'] );
     }
   }
}
add_action( 'admin_init', 'gemscape_acf_check' );

function slug_get_json( $url ) {
	//GET the remote site
	$response = wp_remote_get( $url );

	//Check for error
	if ( is_wp_error( $response ) ) {
		return sprintf( 'The URL %1s could not be retrieved.', $url );
	}

	//get just the body
	$data = wp_remote_retrieve_body( $response );

	//return if not an error
	if ( ! is_wp_error( $data )  ) {

		//decode and return
		return json_decode( $data );

	}

}

if( function_exists('acf_add_options_page') ) {
 	$page = acf_add_options_page(array(
 		'page_title' 	=> 'Gemscape Settings',
 		'menu_title' 	=> 'Gemscape Settings',
 		'menu_slug' 	=> 'gemscape-settings',
 		'capability' 	=> 'activate_plugins',
     ));

     if( function_exists('acf_add_local_field_group') ):
      acf_add_local_field_group(array ( 'title' => 'Gemscape Settings',
      	'key' => 'group_554dee053593d',
      	'fields' => array (
      		array (
      			'key' => 'field_554dee19e8507',
      			'label' => 'Site Type',
      			'name' => 'gemscape_site_type',
      			'type' => 'radio',
      			'instructions' => 'Does the main content of your network live on this site? (Choose Boss), or does this site only display part of the main content (Choose Worker).',
      			'required' => 1,
      			'choices' => array (
      				'boss' => 'Boss',
      				'worker' => 'Worker',
      			),
      			'other_choice' => 0,
      			'save_other_choice' => 0,
      			'default_value' => 'worker : Worker',
      			'layout' => 'horizontal',
      		)
      	),
      	'location' => array (
      		array (
      			array (
      				'param' => 'options_page',
      				'operator' => '==',
      				'value' => 'gemscape-settings',
      			),
      		),
      	),
      	'menu_order' => 1,
      	'position' => 'normal',
      	'style' => 'default',
      	'label_placement' => 'top',
      	'instruction_placement' => 'label',
      ));

      endif;

   global $acf;
   $site_type = get_field('gemscape_site_type', 'option');
   if($site_type == 'boss'){
     $gemscape_includes = [
       'gemscape-boss.php',
       'lib/post-types.php',
       'lib/extras.php',          // Custom functions
       'lib/acf-fields/acf-fields.php'
     ];
   } else {
     $gemscape_includes = [
       'gemscape-worker.php',
       'lib/templates.php',
       'lib/acf-fields/acf-fields.php',
       'lib/sections.php',
       'lib/actions.php'
     ];
   }

   foreach ($gemscape_includes as $file) {
     require_once $file;
   }
}
