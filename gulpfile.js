var gulp = require('gulp');

var paths = {
 scripts: ['./**/*.*', '!./.git', '!./node_modules', '!./gulpfile.js', '!./.gitignore', '!./composer.json'],
 dist = '/Users/admin/Code/worldscape.co/site/web/app/plugins/gemscape';
};

gulp.task('default', function(){
 gulp.src(paths.scripts)
 .pipe(gulp.dest(paths.dist));
});
