<?php
class acf_new_fields {

	function __construct()
	{
		add_action('acf/include_field_types', array($this, 'include_new_field_types') );
	}

	function include_new_field_types() {
		include_once('range-v5.php');
		include_once('limiter-v5.php');
	}
}

new acf_new_fields();

/**
Save & load Gem fields from within the plugin
**/
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
    // update path
		$path = dirname(__FILE__) . '/acf-json';
    // return
    return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
    // remove original path (optional)
    unset($paths[0]);
    // append path
    $paths[] = dirname(__FILE__) . '/acf-json';
    // return
    return $paths;
}
