(function() {
	tinymce.PluginManager.add('gems', function( editor, url ) {
		var sh_tag = 'gems';
		var url = gemscape_details.boss_url;
		url += '/wp-json/taxonomies';
		var areas_list = gemscape_details.areas_list;
		var gem_types_list = gemscape_details.gem_types_list;

		//add popup
		editor.addCommand('gems_popup', function(ui, v) {
			//setup defaults
			var name = '';
			if (v.name)
				name = v.name;
			var gem_type = '';
			if (v.gem_type)
				gem_type = v.gem_type;
			var areas = '';
			if (v.areas)
				areas = v.areas;

			editor.windowManager.open( {

				title: 'Gems Display Shortcode',
				body: [
					{
						type: 'textbox',
						name: 'name',
						label: 'Add by name/slug',
						value: name,
						tooltip: 'Leave blank for none'
					},
					{
						type: 'listbox',
						name: 'gem_type',
						label: 'Gem Type',
						value: gem_type,
						'values': gem_types_list,
						tooltip: 'Select the type of gems to display'
					},
          {
						type: 'listbox',
						name: 'areas',
						label: 'Gem Areas',
						value: areas,
						'values': areas_list,
						tooltip: 'Override the site\'s default Areas'
					},
				],
				onsubmit: function( e ) {
					var shortcode_str = '[' + sh_tag + ' name="'+e.data.name+'"';
					//check for areas
					if (typeof e.data.areas != 'undefined' && e.data.areas.length)
						shortcode_str += ' areas="' + e.data.areas + '"';
					//check for gem_type
					if (typeof e.data.gem_type != 'undefined' && e.data.gem_type.length)
						shortcode_str += ' gem_type="' + e.data.gem_type + '"';

          shortcode_str += ']';

					//insert shortcode to tinymce
					editor.insertContent( shortcode_str);
				}
			});
	      	});

		//add button
		editor.addButton('gems', {
			icon: 'gems',
			tooltip: 'Gems Display',
			onclick: function() {
				editor.execCommand('gems_popup','',{
					areas : '',
					gem_type : '',
					name   : '',
				});
			}
		});

	});
})();
