<?php

class Gemscape_shortcode{
	/**
	 * $shortcode_tag
	 * holds the name of the shortcode tag
	 * @var string
	 */
	public $shortcode_tag = 'gems';

	/**
	 * __construct
	 * class constructor will set the needed filter and action hooks
	 *
	 * @param array $args
	 */
	function __construct($args = array()){
		//add shortcode
		add_shortcode( $this->shortcode_tag, array( $this, 'shortcode_handler' ) );

		if ( is_admin() ){
			add_action('admin_head', array( $this, 'admin_head') );
			add_action( 'admin_enqueue_scripts', array($this , 'admin_enqueue_scripts' ) );
		}
	}

	/**
	 * shortcode_handler
	 * @param  array  $atts shortcode attributes
	 * @param  string $content shortcode content
	 * @return string
	 */
	function shortcode_handler($atts , $content = null){
		// Attributes
		extract( shortcode_atts(
			array(
				'gem_type' => '',
				'areas' => '',
				'name' => '',
			), $atts )
		);

    $boss_url = get_field('gemscape_boss_url', 'option');
		$home_url = get_home_url();
     if($boss_url == NULL){$boss_url = $home_url();}
		$gem_type_query_url = $boss_url . '/wp-json/taxonomies/gem_type/terms';
			if(empty($areas)){$areas_array = get_field('gemscape_worker_areas', 'option'); $areas = implode(",", $areas_array);}
    $gems_query_url = $boss_url . '/wp-json/posts?type=gem&filter[area]='.$areas.'&filter[gem_type]='.$gem_type.'&filter[name]='. $name;
    $requested_gems = slug_get_json($gems_query_url);

		$remote_gem_types = slug_get_json($gem_type_query_url);
		$remote_gem_type_parents = array();
		$remote_gem_type_children = array();

			foreach ($remote_gem_types as $remote_gem_type){
				$gem_type_set = array();
				if(isset($remote_gem_type->parent->slug)){
					$child_slug = $remote_gem_type->slug;
					$parent_slug = $remote_gem_type->parent->slug;
					$gem_type_name = $remote_gem_type->name;
					$gem_type_set[] = $parent_slug;
					$gem_type_set[] = $child_slug;
					$gem_type_set[] = $gem_type_name;
					$remote_gem_type_children[] = $gem_type_set;
				}
				else {
					$parent_slug = $remote_gem_type->slug;
					$remote_gem_type_parents[] = $parent_slug;
				}
			};

			$return_string = '<main class="archive_gems section cd-main-content">';

			if (!empty($gem_type)){
				if(in_array($gem_type, $remote_gem_type_parents)){
					$return_string .= '<div class="cd-tab-filter-wrapper">';
						$return_string .= '<div class="cd-tab-filter"><ul class="cd-filters">';
							$return_string .= '<li class="placeholder"><a data-type="all" href="#0">Everything</a> <!-- selected option on mobile --></li><li class="filter"><a class="selected" href="#0" data-type="all">All</a></li>';
							foreach($remote_gem_type_children as $child_set){
								$parent_slug = $child_set[0];
								if($parent_slug == $gem_type){
									$return_string .= '<li class="filter" data-filter=".'.$child_set[1].'"><a href="#0" data-type="'.$child_set[1].'">'.$child_set[2].'</a></li>';
								}
							}
						$return_string .= '</ul> <!-- cd-filters --> </div> <!-- cd-tab-filter -->';
					$return_string .= '</div> <!-- cd-tab-filter-wrapper -->';
				}
			}

			$return_string .= '<section class="cd-gallery">';
				$return_string .= '<ul>';
				foreach($requested_gems as $gem){
					$gem_types = $gem->terms->gem_type;
					$gem_type_slugs = array();
					foreach($gem_types as $gem_type_object){
						$slug = $gem_type_object->slug;
						$gem_type_slugs[] = $slug;
					}
						$slugs = join( " ", $gem_type_slugs );

					$return_string .= '<li class="mix '.$slugs.'">';
						$return_string .= '<figure class="slide-link gem_small">';
							$gem_image = "";
							if(isset($gem->featured_image->attachment_meta->sizes->gemscape_thumb->url)){
								$gem_image = $gem->featured_image->attachment_meta->sizes->gemscape_thumb->url;
							}
							if(!empty( $gem_image )){
								$return_string .= '<img src="'.$boss_url.'/'.$gem_image.'" />';
							}else {
								$title = $gem->title;
								$stringtitle = str_replace(" ", "+", $title);
								$return_string .= '<img src="http://placehold.it/200x130&text='.$stringtitle.'" />';
							}
							$return_string .= '<figcaption>';
								$return_string .= '<h3>'.$gem->title.'</h3>';
								$return_string .= '<div class="meta">';
									$return_string .= '<span class="gem-type icon-gem"></span>';
								$return_string .= '</div>';
								$return_string .= '<a href="'.$home_url.'gem/'.$gem->slug.'" title="Read More about '.$gem->title.'">Expand</a>';
							$return_string .= '</figcaption>';
						$return_string .= '</figure>';
					$return_string .= '</li>';
				}
					$return_string .='<li class="gap"></li><li class="gap"></li><li class="gap"></li>';
				$return_string .='</ul>';
			$return_string .='</section>';

		$return_string .='</main>';

    return $return_string;	//start panel markup
	}

	/**
	 * admin_head
	 * calls your functions into the correct filters
	 * @return void
	 */
	function admin_head() {
		// check user permissions
		if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
			return;
		}

		// check if WYSIWYG is enabled
		if ( 'true' == get_user_option( 'rich_editing' ) ) {
			add_filter( 'mce_external_plugins', array( $this ,'mce_external_plugins' ) );
			add_filter( 'mce_buttons', array($this, 'mce_buttons' ) );

      $boss_url = get_field('gemscape_boss_url', 'option');
			$areas_url = $boss_url . '/wp-json/taxonomies/area/terms';
	     $areas_json = slug_get_json($areas_url);
			$gem_types_url = $boss_url . '/wp-json/taxonomies/gem_type/terms';
	     $gem_types_json = slug_get_json($gem_types_url);
      ?>
      <!-- TinyMCE Shortcode Plugin -->
      <script type='text/javascript'>
      var gemscape_details = {
          'boss_url': '<?php echo $boss_url; ?>',
          'areas_list': [{text: 'None', value: ''},
						<?php foreach($areas_json as $area){
							$area_slug = $area->slug;
							echo '{text: \''.$area_slug.'\', value: \''.$area_slug.'\'},';
						}?>
          ],
					'gem_types_list': [{text: 'None', value: ''},
						<?php foreach($gem_types_json as $gem_type){
							$gem_type_slug = $gem_type->slug;
							echo '{text: \''.$gem_type_slug.'\', value: \''.$gem_type_slug.'\'},';
						}?>
          ],
      };
      </script>
      <!-- TinyMCE Shortcode Plugin -->
		<?php }
	}

	/**
	 * mce_external_plugins
	 * Adds our tinymce plugin
	 * @param  array $plugin_array
	 * @return array
	 */
	function mce_external_plugins( $plugin_array ) {
		$plugin_array[$this->shortcode_tag] = plugins_url( 'js/mce-button.js' , __FILE__ );
		return $plugin_array;
	}

	/**
	 * mce_buttons
	 * Adds our tinymce button
	 * @param  array $buttons
	 * @return array
	 */
	function mce_buttons( $buttons ) {
		array_push( $buttons, $this->shortcode_tag );
		return $buttons;
	}

	/**
	 * admin_enqueue_scripts
	 * Used to enqueue custom styles
	 * @return void
	 */
	function admin_enqueue_scripts(){
		wp_enqueue_style('gemscape_shortcode', plugins_url( 'css/mce-button.css' , __FILE__ ) );
	}

}//end class

new Gemscape_shortcode();

function frontend_enqueue_scripts(){
	wp_enqueue_style('gems_display', plugins_url( 'css/gemscape.css' , __FILE__ ) );
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//code.jquery.com/jquery-1.11.3.min.js', false, '1.11.3');
		wp_enqueue_script('jquery');
	}
	wp_enqueue_script('gemscape_filter', plugins_url('js/filter.js', __FILE__));
	wp_enqueue_script('gemscape_mixitup', plugins_url('js/jquery.mixitup.min.js', __FILE__), array('jquery'));
}
add_action( 'wp_enqueue_scripts', 'frontend_enqueue_scripts' );
