<?php
/**
Page Templates via http://www.wpexplorer.com/wordpress-page-templates-plugin/
**/

class PageTemplater {
  protected $plugin_slug;
  private static $instance;
  protected $templates;
  public static function get_instance() {
    if( null == self::$instance ) {
      self::$instance = new PageTemplater();
    }
    return self::$instance;
  }

  private function __construct() {
    $this->templates = array();
    // Add a filter to the attributes metabox to inject template into the cache.
    add_filter(
      'page_attributes_dropdown_pages_args',
      array( $this, 'register_project_templates' )
    );
    // Add a filter to the save post to inject out template into the page cache
    add_filter(
      'wp_insert_post_data',
      array( $this, 'register_project_templates' )
    );
    // Add a filter to the template include to determine if the page has our
    // template assigned and return it's path
    add_filter(
      'template_include',
      array( $this, 'view_project_template')
    );
    // Add your templates to this array.
    $this->templates = array(
      'template-area.php'     => 'Area Page',
    );
  }

  public function register_project_templates( $atts ) {
    $theme = wp_get_theme();
    $cache_key = 'page_templates-' . md5( $theme->get_theme_root() . '/' . $theme->get_stylesheet() );
    $templates = $theme->get_page_templates();
    $templates = array_merge( $templates, $this->templates );
    wp_cache_set( $cache_key, $templates, 'themes', 1800 );
    return $atts;
  }

  public function view_project_template( $template ) {
    global $post;
    if (!isset($this->templates[get_post_meta(
      $post->ID, '_wp_page_template', true
    )] ) ) {
      return $template;
    }
    $file = plugin_dir_path(__FILE__). get_post_meta(
      $post->ID, '_wp_page_template', true
    );
    if( file_exists( $file ) ) {
      return $file;
    }
    else { echo $file; }
    return $template;
  }
}

add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );

/**
Area Page Fields
**/

function add_area_page_fields(){
if( function_exists('acf_add_local_field_group') ):
  $boss_url = get_field('gemscape_boss_url', 'option');
  $home_url = get_home_url();
  if($boss_url == NULL){$boss_url = $home_url();}
  $area_json_url = $boss_url . '/wp-json/taxonomies/area/terms/';
		$areas = slug_get_json($area_json_url);
	  $site_areas = array();
	  foreach( $areas as $area ) {
	    $site_areas[$area->slug] = $area->slug;
	  }
    $site_areas[''] = '';

acf_add_local_field_group(array (
	'key' => 'group_5556d0f974296',
	'title' => 'Area Page Fields',
	'fields' => array (
		array (
			'key' => 'field_5556d463a83c8',
			'label' => 'Area Splash Image',
			'name' => 'gemscape_area_splash',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
    array(
      'key' => 'field_5556d463a84c9',
      'label' => 'Area Thumbnail Image',
      'name' => 'gemscape_area_thumb',
      'type' => 'image',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'return_format' => 'array',
      'preview_size' => 'thumbnail',
      'library' => 'all',
      'min_width' => '',
      'min_height' => '',
      'min_size' => '',
      'max_width' => '',
      'max_height' => '',
      'max_size' => '',
      'mime_types' => '',
    ),
		array (
			'key' => 'field_5556d4e1a83c9',
			'label' => 'Area Mini Map',
			'name' => 'gemscape_area_mini_map',
			'type' => 'image',
			'instructions' => 'png file please',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
    array (
			'key' => 'field_54f2e58bebb8b',
			'label' => 'Short Description',
			'name' => 'gemscape_area_short_description',
			'prefix' => '',
			'type' => 'limiter',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'character_number' => 175,
			'display_characters' => 0,
		),
    array (
			'key' => 'field_5556da75a83ce',
			'label' => 'Area Longer Description',
			'name' => 'gemscape_longer_description',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => '',
			'new_lines' => 'wpautop',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_5556d931a83cd',
			'label' => 'Worldscape Area',
			'name' => 'worldscape_area',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => $site_areas,
			'default_value' => array (
				'' => '',
			),
			'allow_null' => 0,
			'multiple' => 1,
			'ui' => 1,
			'ajax' => 1,
			'placeholder' => '',
			'disabled' => 0,
			'readonly' => 0,
		),
		array (
			'key' => 'field_5556d5aaa83cc',
			'label' => 'Sub-Areas',
			'name' => 'gemscape_area_subareas',
			'type' => 'post_object',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'page',
			),
			'taxonomy' => array (
			),
			'allow_null' => 0,
			'multiple' => 1,
			'return_format' => 'object',
			'ui' => 1,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'page_template',
				'operator' => '==',
				'value' => 'template-area.php',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array (
		0 => 'author',
	),
));

endif;
}
add_action( 'init', 'add_area_page_fields', 2 );
