<?php

if( function_exists('acf_add_local_field_group') ):

	$boss_url = get_field('gemscape_boss_url', 'option');
  $home_url = get_home_url();
  if($boss_url == NULL){$boss_url = $home_url();}
  $area_json_url = $boss_url . '/wp-json/taxonomies/area/terms/';
		$areas = slug_get_json($area_json_url);
	  $site_areas = array();
	  foreach( $areas as $area ) {
	    $site_areas[$area->slug] = $area->slug;
	  }
    $site_areas[''] = '';
	$gem_type_json_url = $boss_url . '/wp-json/taxonomies/gem_type/terms/';
		$gem_types = slug_get_json($gem_type_json_url);
	  $site_gem_types = array();
	  foreach( $gem_types as $gem_type ) {
	    $site_gem_types[$gem_type->slug] = $gem_type->slug;
	  }
    $site_gem_types[''] = '';


acf_add_local_field_group(array ('title' => 'Sections',
	'key' => 'group_5563f7d2b752d',
	'fields' => array (
		array ('label' => 'Page Sections',
			'key' => 'field_5563f7dfecec7',
			'name' => 'gemscape_page_sections',
			'type' => 'flexible_content',
			'button_label' => 'Add Section',
			'layouts' => array (
				array ('label' => 'Area Overview',
					'key' => '5563f7ea57fcf',
					'name' => 'area_overview',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_5563f89cecec8',
							'label' => 'Section Title',
							'name' => 'section_title',
							'type' => 'text',
							'default_value' => 'Explore by Area',
						),
						array (
							'key' => 'field_5563f8d9ecec9',
							'label' => 'Container Areas',
							'name' => 'container_areas',
							'type' => 'repeater',
							'layout' => 'table',
							'button_label' => 'Add Container Area',
							'sub_fields' => array (
								array (
									'key' => 'field_55640ef69ac79',
									'label' => 'Container Area Page Link',
									'name' => 'container_area_page_link',
									'type' => 'post_object',
									'post_type' => array (
										0 => 'page',
									),
									'allow_null' => 0,
									'multiple' => 0,
									'return_format' => 'object',
									'ui' => 1,
								),
								array (
									'key' => 'field_5563f905ececa',
									'label' => 'Container Area Name',
									'name' => 'container_area_name',
									'type' => 'text',
								),
								array (
									'key' => 'field_5563f92fececb',
									'label' => 'Container Area Description',
									'name' => 'container_area_description',
									'type' => 'textarea',
								),
								array (
									'key' => 'field_5563fa51ecece',
									'label' => 'Container Area Main Image',
									'name' => 'container_area_main_image',
									'type' => 'image',
									'return_format' => 'array',
									'preview_size' => 'thumbnail',
									'library' => 'all',
								),
								array (
									'key' => 'field_5563f95cececc',
									'label' => 'Container Area Mini Map',
									'name' => 'container_area_mini_map',
									'type' => 'image',
									'return_format' => 'array',
									'preview_size' => 'thumbnail',
									'library' => 'all',
								),
								array (
									'key' => 'field_5563f981ececd',
									'label' => 'Container Area Sub Areas',
									'name' => 'container_area_sub_areas',
									'type' => 'post_object',
									'allow_null' => 0,
									'multiple' => 1,
									'return_format' => 'object',
									'ui' => 1,
								),
							),
						),
					),
				),
				array ('label' => 'Area Page Header',
					'key' => '5563fb90ececf',
					'name' => 'area_page_header',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_5563fba5eced0',
							'label' => 'Area Mini Map',
							'name' => 'area_mini_map',
							'type' => 'image',
							'wrapper' => array (
								'width' => 50,
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
						),
						array (
							'key' => 'field_5563fc06eced1',
							'label' => 'Area Splash Image',
							'name' => 'area_splash_image',
							'type' => 'image',
							'wrapper' => array (
								'width' => 50,
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
						),
						array (
							'key' => 'field_5563fc1ceced2',
							'label' => 'Area Description',
							'name' => 'area_description',
							'type' => 'textarea',
						),
						array (
							'key' => 'field_5563fd6eeced5',
							'label' => 'Sub Areas Title',
							'name' => 'sub_areas_title',
							'type' => 'text',
							'default_value' => 'Main Areas',
						),
						array (
							'key' => 'field_5563fc4aeced3',
							'label' => 'Sub Areas',
							'name' => 'sub_areas',
							'type' => 'post_object',
							'post_type' => array (
								0 => 'page',
							),
							'allow_null' => 0,
							'multiple' => 1,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
				),
				array ('label' => 'Gem Display',
					'key' => '5563fdf7eced6',
					'name' => 'gem_display',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_5563ff1aecedb',
							'label' => 'Gem Display TItle',
							'name' => 'gem_display_title',
							'type' => 'text',
							'default_value' => 'What\'s Here',
						),
						array (
							'key' => 'field_5563fe0ceced7',
							'label' => 'Gem Display Type',
							'name' => 'gem_display_type',
							'type' => 'checkbox',
							'choices' => array (
								'gem_type' => 'Gem Type',
								'area' => 'Area',
								'individual' => 'Individual',
							),
							'default_value' => array (
								'' => '',
							),
							'layout' => 'horizontal',
						),
						array (
							'key' => 'field_5563fe79eced8',
							'label' => 'Gem Type Choices',
							'name' => 'gem_type_choices',
							'type' => 'select',
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_5563fe0ceced7',
										'operator' => '==',
										'value' => 'gem_type',
									),
								),
							),
							'choices' => $site_gem_types,
							'default_value' => array (
								'' => '',
							),
							'allow_null' => 0,
							'multiple' => 1,
							'ui' => 1,
							'ajax' => 0,
							'placeholder' => '',
						),
						array (
							'key' => 'field_5563fec0eced9',
							'label' => 'Area Choices',
							'name' => 'area_choices',
							'type' => 'select',
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_5563fe0ceced7',
										'operator' => '==',
										'value' => 'area',
									),
								),
							),
							'choices' => $site_areas,
							'default_value' => array (
								'' => '',
							),
							'allow_null' => 0,
							'multiple' => 1,
							'ui' => 1,
							'ajax' => 0,
							'placeholder' => '',
						),
						array (
							'key' => 'field_5563fedbeceda',
							'label' => 'Individual Choices',
							'name' => 'individual_choices',
							'type' => 'select',
							'conditional_logic' => array (
								array (
									array (
										'field' => 'field_5563fe0ceced7',
										'operator' => '==',
										'value' => 'individual',
									),
								),
							),
							'choices' => $site_areas,
							'default_value' => array (
								'' => '',
							),
							'allow_null' => 0,
							'multiple' => 1,
							'ui' => 1,
							'ajax' => 1,
							'placeholder' => '',
						),
					),
				),
				array ('label' => 'Gem Type Overview',
					'key' => '5563ff49ecedc',
					'name' => 'gem_type_overview',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_5563ff84ecedd',
							'label' => 'Section Title',
							'name' => 'section_title',
							'type' => 'text',
						),
						array (
							'key' => 'field_5563ffa0ecede',
							'label' => 'Gem Type Link',
							'name' => 'gem_type_link',
							'type' => 'post_object',
							'post_type' => array (
								0 => 'page',
							),
							'allow_null' => 0,
							'multiple' => 1,
							'return_format' => 'object',
							'ui' => 1,
						),
					),
				),
				array ('label' => 'Blog Section',
					'key' => '5564c8fe4aca2',
					'name' => 'blog_section',
					'display' => 'row',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'label_placement' => 'top',
));

endif;

function sections_loop() {
	global $wp_query;
	$object = $wp_query->get_queried_object();
	$object_id = $object->ID;
	// check if the flexible content field has rows of data
if( have_rows('gemscape_page_sections', $object_id) ):
    while ( have_rows('gemscape_page_sections', $object_id) ) : the_row();
        if( get_row_layout() == 'area_overview' ):
					$output = '<div class="area_list section">';
					$output .= '<h2>'.get_sub_field('section_title').'</h2>';
					if(have_rows('container_areas')):
						while ( have_rows('container_areas') ) : the_row();
									$container_area_page_link = get_sub_field('container_area_page_link');
									$container_area_name = get_sub_field('container_area_name');
									$container_area_description = get_sub_field('container_area_description');
									$container_area_main_image = get_sub_field('container_area_main_image');
									$container_area_mini_map = get_sub_field('container_area_mini_map');
									$container_area_sub_areas = get_sub_field('container_area_sub_areas');

										$url = get_permalink($container_area_page_link->ID);
										$size = 'gemscape_thin';
										if(empty($container_area_name)){
											$container_area_name = $container_area_page_link->title;
										}
										if(empty($container_area_main_image)){
											$image_id = get_post_thumbnail_id($container_area_page_link->ID);
											$image =  wp_get_attachment_image_src( $image_id, $size);
											$thumb = $image[0];
											$width = $image[1];
											$height = $image[2];
										}else{
											$thumb = $container_area_main_image['sizes'][ $size ];
											$width = $container_area_main_image['sizes'][ $size . '-width' ];
											$height = $container_area_main_image['sizes'][ $size . '-height' ];
										};

							$output .= '<div class="area">';
								$output .= '<a href="'.$url.'" title="'.$container_area_name.'">';

								$output .= '<img class="area-image" src="'.$thumb.'" alt="'.$container_area_name.'" width="'.$width.'" height="'.$height.'" />';
								$output .= '</a>';
								$output .= '<div class="area-info">';
								$output .= '<img class="small-map" src="'.$container_area_mini_map['url'].'" />';
								$output .= '<h3>'.$container_area_name.'</h3>';
									if( $container_area_sub_areas ){
										$output .= '<div class="sub-areas">';
										foreach( $container_area_sub_areas as $sub_area){
											$output .= '<a class="standard" href="' . get_permalink($sub_area->ID) . '">' . get_the_title($sub_area->ID) . '</a>';
										};
										$output .= '</div>';
									};
								$output .= '<p>'.$container_area_description.'</p>';
								$output .= '<div class="arrow-left"></div>';
								$output .= '<a href="'.$url.'" title="Click to Read More About '.$container_area_name.'">View More</a>';
								$output .= '</div>';
							$output .= '</div>';
						endwhile;
					endif;
					$output .= '</div>';
        	echo $output;
        elseif( get_row_layout() == 'area_page_header' ):
        	$file = get_sub_field('file');
				elseif( get_row_layout() == 'gem_type_overview' ):
					$menu_name = 'gem_type_menu';

					if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
						$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
						$menu_items = wp_get_nav_menu_items($menu->term_id);

						$menu_list = '<div class="gem_type_list section">';
							$menu_list .= '<h2>Explore by Category</h2>>';
							$sub_menu_list = array();
						foreach ( $menu_items as $menu_item ) {
							$menu_parent = $menu_item->menu_item_parent;
							$title = $menu_item->title;
							$url = $menu_item->url;
							if($menu_parent != 0){$sub_menu_list[] = array($menu_parent, $title, $url);}
						}
						foreach ( $menu_items as $menu_item ) {
						    $title = $menu_item->title;
						    $url = $menu_item->url;
						    $description = $menu_item->description;
						    $menu_icon = $menu_item->classes[0];
								$menu_item_id = $menu_item->ID;
								$menu_parent = $menu_item->menu_item_parent;

								if ($menu_parent == 0) {
							    $menu_list .= '<div class="gem_type">';
										$menu_list .= '<div class="gem_type_info">';
											$menu_list .= '<h3>'.$title.'</h3>';
											$menu_list .= '<div class="sub_gem_types">';
												$menu_list .= '<a class="standard" href="'.$url.'">All</a>';
												foreach($sub_menu_list as $sub_menu_item){
													if ($sub_menu_item[0] == $menu_item_id){
														$menu_list .= '<a class="standard" href="' . $sub_menu_item[2] . '">' . $sub_menu_item[1] . '</a>';
													}
												}
											$menu_list .= '</div>';
											$menu_list .= '<a href="'. $url .'" title="Explore in '.$title.'">View More</a>';
										$menu_list .= '</div>';
									$menu_list .= '</div>';
								} else {}
							}
						$menu_list .= '</div>';

						echo $menu_list;
					};
			//	elseif( get_row_layout() == 'map_overview' ):
        //	$file = get_sub_field('file');
				elseif( get_row_layout() == 'gem_display' ):
					echo '<div class="gem_display section"><h2>'.get_sub_field('gem_display_title').'</h2>';
					if( in_array( 'gem_type', get_sub_field('gem_display_type') ) ) {
						$gem_types = implode(', ', get_sub_field('gem_type_choices'));
						echo do_shortcode('[gems gem_type="'.$gem_types.'"]');
					} elseif (in_array( 'area', get_sub_field('gem_display_type') ) ) {
						$gem_areas = implode(', ', get_sub_field('area_choices'));
		        echo do_shortcode('[gems areas="'.$gem_areas.'"]');
					}
					echo '</div>';
				elseif( get_row_layout() == 'blog_section' ):
					$args2 = array(
							'post_type'   => 'post',
							'posts_per_page' => 3
						);
					$query2 = new WP_Query( $args2 );
					if($query2->have_posts()){?>
						<div class="blog section">
							<h2>Blog</h2>
							<div class="blog_posts">
					<?php while ( $query2->have_posts() ) : $query2->the_post(); ?>
									<div class="single_post">
										<a href="<?php the_permalink( $query2->post->ID )?>">
											<img src="http://placehold.it/200x130" />
											<h4><?php echo get_the_title( $query2->post->ID );?></h4>
											<div class="post_meta">
												<span>29 Shares</span> | <a href="#">Trip Ideas</a>, <a href="#">Day Tours</a>
											</div>
											<p><?php the_excerpt( $query2->post->ID );?></p>
										</a>
									</div>
							<?php endwhile; wp_reset_postdata();?>
								</div>
								<div class="blog_categories">
									<h3>Categories</h3>
									<ul>
										<li><a href="#">One</a></li>
										<li><a href="#">Two</a></li>
										<li><a href="#">Three</a></li>
									</ul>
								</div>
							</div>
<?php }
        endif;
    endwhile;
endif;
}
add_action('gemscape_sections', 'sections_loop');
