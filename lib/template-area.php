<?php while (have_posts()) : the_post(); ?>

  <?php
  	$splash = get_field('gemscape_area_splash');
      if ( !empty($splash)) {
  		$size = 'gemscape_long';
  		$thumb = $splash['sizes'][ $size ];
      } else{
          $thumb = 'http://placehold.it/1000x340&text='.get_the_title();
      }
  	?>
  <div class="area-header section" style="background-image:url('<?php echo $thumb;?>');background-position:center top;" >
  	<div class="area-basics">
  		<img class="small-map" src="<?php echo get_field('gemscape_area_mini_map');?>" />
  		<h1><?php the_title();?></h1>
  		<p class="description"><?php echo get_field('gemscape_area_longer_description');?></p>
  	</div>
  </div>
  <div class="area-content section">
  		<?php
  		$subareas = get_field('gemscape_area_subareas');
  		if(!empty($subareas)){?>
  			<div class="area_main_towns section">
  				<h2>Main Areas</h2>
  				<?php
          var_dump($subareas);
  				foreach ( $subareas as $subarea ) {
  					$url = get_page_link( $subarea, 'page' );
  					$term2 = get_post('id', $subarea, 'page' );
  					$image = get_field('gemscape_area_main_image', $term2);
  					?>
  					<figure class="small-town slide-link">
  						<?php
  						if( !empty($image) ){
  							$title = $image['title'];
  							$alt = $image['alt'];
  							$caption = $image['caption'];

  							// thumbnail
  							$size = 'gemscape_thumb_thin';
  							$thumb = $image['sizes'][ $size ];
  							$width = $image['sizes'][ $size . '-width' ];
  							$height = $image['sizes'][ $size . '-height' ];
  							?>
  							<img class="small-town-image" src="<?php echo $thumb; ?>" alt="<?php echo $term2->name; ?> on The Ring of Beara" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
  						<?php } else{
  				            $term_title = $term2->name;
  				            $stringtitle = str_replace(" ", "+", $term_title);?>
  				            <img class="small-town-image" src="http://placehold.it/320x130&text=<?php echo $stringtitle;?>" alt="<?php echo $term2->name; ?> on The Ring of Beara" width="320" height="130" />
  				       <?php }?>
  						<figcaption>
  							<h3><?php echo $term2->name;?></h3>
  							<p><span class="icon-info">Learn More</span></p>
  							<?php $mini_map = get_field('gemscape_area_mini_map', $term);
  							if(!empty($mini_map))
  								{}
  								else{$mini_map = get_field('gemscape_area_mini_map', $term);
  								}?>
  							<img class="small-map" src="<?php echo $mini_map;?>" />
  							<a href="<?php echo $url;?>" title="Read More about <?php echo $term2->name;?>">View More</a>
  						</figcaption>
  					</figure>
  				<?php } ?>
  			</div>
  		<?php }?>


<?php the_content();?>

<?php endwhile; ?>
