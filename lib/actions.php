<?php
function create_gem_type_pages() {
// Get the gem types list from the json connection.
$boss_url = get_field('gemscape_boss_url', 'option');
$home_url = get_home_url();
 if($boss_url == NULL){$boss_url = $home_url();}
$gem_type_query_url = $boss_url . '/wp-json/taxonomies/gem_type/terms';

$menu_exists = wp_get_nav_menu_object( 'Categories' );
if( !$menu_exists){ $menu_id = wp_create_nav_menu('Categories'); } else { $menu_id = $menu_exists->term_id; }

$remote_gem_types = slug_get_json($gem_type_query_url);
$gem_type_parents_mix = array();
  foreach ($remote_gem_types as $remote_gem_type){
    $gem_type_name = $remote_gem_type->name;
    $gem_type_slug = $remote_gem_type->slug;
    $gem_type_id = $remote_gem_type->ID;
    if(!isset($remote_gem_type->parent->slug)){
      $gem_type_page = array(
        'post_name'      => $gem_type_slug,
        'post_title'     => $gem_type_name,
        'post_status'    => 'publish',
        'post_type'      => 'page',
      );
      if( null == get_page_by_title( $gem_type_name ) ) {
        $gem_type_page_id = wp_insert_post($gem_type_page);
      } else {
        $gem_type_page_object = get_page_by_title( $gem_type_name );
        $gem_type_page_id = $gem_type_page_object->ID;
      }

      $gem_type_page_link = get_page_link($gem_type_page_id);
      $menu_item = array(
        'menu-item-title' =>  $gem_type_name,
        'menu-item-classes' => $gem_type_slug,
        'menu-item-url' => $gem_type_page_link,
        'menu-item-status' => 'publish'
      );

      $items = wp_get_nav_menu_items( $menu_id );
      $item_titles = array();
      foreach($items as $item){
        $item_titles[] = $item->title;
      }
      if( !in_array($gem_type_name, $item_titles)) {
        $menu_item_id = wp_update_nav_menu_item($menu_id, 0, $menu_item);
      } else {
        foreach($items as $item){
          if ($gem_type_name == $item->title) { $menu_item_id = $item->ID; }
        }
      }

      $gem_type_parents_mix[] = array($gem_type_id, $gem_type_page_id, $menu_item_id);
    }
  }
  foreach ($remote_gem_types as $remote_gem_type){
    $gem_type_name = $remote_gem_type->name;
    $gem_type_slug = $remote_gem_type->slug;
    $gem_type_id = $remote_gem_type->ID;
    if(isset($remote_gem_type->parent->slug)){
      $gem_type_parent_id = $remote_gem_type->parent->ID;
      foreach($gem_type_parents_mix as $gem_type_parent_mix){
        if(in_array($gem_type_parent_id, $gem_type_parent_mix)){
          $gem_type_parent = $gem_type_parent_mix[1];
          $gem_type_parent_menu = $gem_type_parent_mix[2];
        }
      }
      $gem_type_page = array(
        'post_name'      => $gem_type_slug,
        'post_title'     => $gem_type_name,
        'post_status'    => 'publish',
        'post_type'      => 'page',
        'post_parent'    => $gem_type_parent
      );
      if( null == get_page_by_title( $gem_type_name ) ) {
        $gem_type_page_id = wp_insert_post($gem_type_page);
      } else {
        $gem_type_page_object = get_page_by_title( $gem_type_name );
        $gem_type_page_id = $gem_type_page_object->ID;
      }

      $gem_type_page_link = get_page_link($gem_type_page_id);
      $menu_item = array(
        'menu-item-title' =>  $gem_type_name,
        'menu-item-classes' => $gem_type_slug,
        'menu-item-url' => $gem_type_page_link,
        'menu-item-parent-id' => $gem_type_parent_menu,
        'menu-item-status' => 'publish'
      );

      $items = wp_get_nav_menu_items( $menu_id );
      $item_titles = array();
      foreach($items as $item){
        $item_titles[] = $item->title;
      }
      if( !in_array($gem_type_name, $item_titles)) {
        $menu_item_id = wp_update_nav_menu_item($menu_id, 0, $menu_item);
      } else {
        foreach($items as $item){
          if ($gem_type_name == $item->title) { $menu_item_id = $item->ID; }
        }
      }
    }
  }

  register_nav_menus( array('gem_type_menu' => __( 'Gem Type Menu' )) );
}
add_action( 'admin_init', 'create_gem_type_pages' );
