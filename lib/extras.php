<?php

namespace HiberniaGems\Extras;

/**
Add Connections between Gems and Sprout Clients
**/

//function my_connection_types() {

//    p2p_register_connection_type( array(
//         'name' => 'gem_to_client',
//         'from' => 'sa_client',
//         'to' => 'gem',
//         'cardinality' => 'one-to-many'
//     ) );
//     p2p_register_connection_type( array(
//         'name' => 'gem_to_invoice',
//         'from' => 'sa_invoice',
//         'to' => 'gem',
//         'cardinality' => 'one-to-many'
//     ) );
//     p2p_register_connection_type( array(
//         'name' => 'gem_to_surl',
//         'from' => 'surl',
//         'to' => 'gem',
//         'cardinality' => 'one-to-one'
//     ) );

// }
// add_action( 'p2p_init',  __NAMESPACE__ . '\\my_connection_types' );

/**
Hide Menu Items for non-admins
**/
function remove_menu_items() {
    if( !current_user_can( 'administrator' ) ):
      remove_menu_page( 'edit.php?post_type=gem' );
      remove_menu_page( 'edit.php?post_type=sa_invoice' );
      remove_menu_page( 'edit.php?post_type=sa_estimate' );
      remove_menu_page('tools.php');
      remove_menu_page('acf-options');
    endif;
}
add_action( 'admin_menu',  __NAMESPACE__ . '\\remove_menu_items' );

/**
Add ACF fields to JSON API
**/
function wp_api_encode_acf($data,$post,$context){
    $customMeta = (array) get_fields($post['ID']);
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
function wp_api_encode_acf_taxonomy($data,$post){
    $customMeta = (array) get_fields($post->taxonomy."_". $post->term_id );
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
function wp_api_encode_acf_user($data,$post){
    $customMeta = (array) get_fields("user_". $data['ID']);
    $data['meta'] = array_merge($data['meta'], $customMeta );
    return $data;
}
add_filter('json_prepare_post', __NAMESPACE__ . '\\wp_api_encode_acf', 10, 3);
add_filter('json_prepare_page', __NAMESPACE__ . '\\wp_api_encode_acf', 10, 3);
add_filter('json_prepare_attachment', __NAMESPACE__ . '\\wp_api_encode_acf', 10, 3);
add_filter('json_prepare_term', __NAMESPACE__ . '\\wp_api_encode_acf_taxonomy', 10, 2);
add_filter('json_prepare_user', __NAMESPACE__ . '\\wp_api_encode_acf_user', 10, 2);

/**
Clean up Gem Output in JSON API
**/

function wp_api_limiter($data,$post,$context){
  unset( $data['excerpt'] );
  unset( $data['link'] );
  unset( $data['guid'] );
  unset( $data['author'] );
  unset( $data['comment_status'] );
  unset( $data['ping_status'] );
  unset( $data['sticky'] );
  unset( $data['content'] );
  unset( $data['parent'] );
  unset( $data['format'] );
  unset( $data['menu_order'] );
  unset( $data['date_tz'] );
  unset( $data['date_gmt'] );
  unset( $data['modified_tz'] );
  unset( $data['modified_gmt'] );
  unset( $data['type'] );
  unset( $data['date'] );
  unset( $data['modified'] );
  unset( $data['meta'] );

  $acf = get_fields($data['ID']);
    if (isset($data)) {
      $data['acf'] = $acf;
    }

  return $data;
}
add_filter('json_prepare_post', __NAMESPACE__ . '\\wp_api_limiter', 10, 3);
